# Overview

The FPGA Station Module (FSM) is a carrier board designed to interface a pair of analog-over-fiber RF signals with an FPGA performing digital signal processing. The FSM emits digital data over high speed (25 or 100Gb/s) Ethernet.

The FSM hosts an FPGA System on Module (SoM), a high speed dual-channel ADC, and a pair of Fiber Receiver (FRX) boards.

A top-level block diagram of the FSM is shown below:

![Block diagram](./diagrams/rcf-fsm-jan2024.drawio.png)


# Physical Specifications

## Summary of Board configuration

1. 100 mm Eurocard height
2. 280 mm Eurocard length
3. Designed for mounting vertically in 3U subrack. Board edges should have adequate keepout zones to accomodate card guide rails.
4. Custom heatsink required for bottom to top airflow
5. Designed to operate with a half-height backplane, with backplane on bottom half of subrack. QSFP/RJ45 connectors and LEDs should be mounted so that they do not interfere with the backplane.
6. Mounting holes to accomodate operation in non-card-cage environments.
7. 8 HP width


### FRX interface

The FRX board top-down view is shown below:

![FRX top-down view](./images/frx-top.png)

The FRX board is installed top-down on the FRX. A view of an FRX, side-on, as installed on the FSM is shown below:

![FRX side-on view](./images/frx-side-facedown.png)

A dimensioned drawing of the FRX board is available [here](./images/D2k-00062-SYS-ICD ASP to RCF.pdf).


1. The FSM-side of the FRX-FSM interface is a catchers mitt surface mount SMP eg [Amphenol SMP-MSCM-PCT-10](https://www.amphenolrf.com/smp-mscm-pct-10.html)

2. Power and control signals are delivered to the FRX via a 2x4 0.1" header. The pinout of the male header on the FRX board is:
	- pin 1: I2C SCL (3.3V)
	- pin 2: I2C SDA (3.3V)
	- pin 3: GND
	- pin 4: VDD (6.5 - 18V) 
	- pin 5: GND
	- pin 6: VDD (6.5 - 18V)
	- pin 7: Not Connected
	- pin 8: Not Connected

3. In operation, an FRX module is expected to draw 200 mA. The FRX has linear power regulators, so the power dissipation is expected to be between 1.3 and 3.6 W, depending on the input voltage (6.5 - 18V).

4. The FSM board should pull up SDA and SCL lines to 3.3V, per the I2C spec.

5. Each of the two FRX boards should have an individual I2C interface to the PS side of the SoM FPGA.

### Backplane interface

The FSM board receives power and control signals from a custom backplane.

Mechanical constraints for interfacing with the backplane are (LRD 2024-06-17, ver 1.0) as follows:

#### Notes

Coordinate system, as mounted in rack and viewed from front of subrack:
  X increases toward back of subrack
  Y increases upward
  Z increases to the right.

The FSM assembly is understood to consist of a carrier board, a mezzaniine board (SoM), and all attached accessories such as heat sinks.

#### Constraints

1.  The FSM carrier board's backplane connector shall be Molex PN 76170-1036, designated J1 in the below constraints.

2.  J1 shall be mounted on the +Z side of the board, designated "top" in the below constraints.

3.  Relative to the lower rear corner of the board, the connector position shall be
      X = -1.50 mm
      Y = 20.00 mm.
    See the drawing [](./images/DummyCarrierBoardDrawing.pdf).

4.  Except for J1, no component shall extend beyond the rear edge of the board (X>0) below 55.0 mm (Y<55.0mm) from the lower edge.  (For Y>55mm, components may extend beyond the rear edge.)

5.  Carrier board thickness shall be no larger than 2.4 mm.

6.  Relative to the center of the board thickness, no component of the FSM assembly shall extend into the space
     Z > 37.37 mm (on the top side), nor
     Z < -8.35 mm (on the bottom side).

7.  The total Z dimension (width) of the FSM assembly, from the tallest component on the top to the tallest component on the bottom, shall not exceed 40.64 mm (=8HP).  (Note that this is smaller than the 45.72 mm that would be allowed by the constraints in item 6.)

#### Backplane connector pinout

![FSM backplane connector pinout](./images/fsm-backplane-pinout.png)


### ADC Interface

The FSM uses Analog Devices AD9207 dual-channel ADCs. See the [AD9082-FMCA-EBZ Schematic](https://www.analog.com/media/en/technical-documentation/eval-board-schematic/ad9081-mxfe-fmca-revc-eval-board-schematic.pdf) for reference.  

Specifically required FSM connections are:

1. SDIO -- SoM PS SPI MOSI
2. SDO -- SoM PS SPI MISO
3. SCLK -- SoM PS SPI SCLK
4. CSB -- SoM PS SPI CSB
5. CLKP -- REFP from backplane
6. CLKN -- REFN from backplane
7. SYSREFP -- SYNCP from backplane
8. SYSREFN -- SYNCN from backplane
9. GPIO0 - GPIO10 -- SoM PS GPIO (via level converters)

### SoM Interface

Some SoM signals shoudl be directly connected to the backplane as follows:

1. SoM Power Enable - PENABLE from backplane. A jumper install option should be provided to allow power enable to be permanently pulled high.
2. SoM FPGA UART0 - micro USB connector
3. SoM FPGA UART1 - UART1 from backplane
4. SoM SD card - micro SD card slot
5. SoM PS GPIO - SLOT0/SLOT1/SLOT2/SLOT3 from backplane
6. SoM PL GPIO, 3V3 capable - "spare" pins on backplane, via 0-ohm resistors
7. SoM PL GPIO, 3V3 capable - 5 x 2 (or similar) 0.1" header

# Functional Requirements

## Backplane interface

1. The FSM board should communicate via 1000-BASEX over the backplane. This interface should be available to the SoM FPGA PS, and may require an external PHY and/or reference clock.

2. The FSM board should communicate via 10/100/1000 BASE-T via an RF45 connector. This interface should be available to the SoM FPGA PS.

3. The FSM is powered from a single supply at 12V, and up to 9A.

4. The backplane I2C interface is connected to a several devices:
	1. I2C GPIO expander, driving:
		1. SoM warm reset PS_SRST_B (Pulled up so as to default to not reset)
		2. 15-bits GPIO to the SoM PS (or as many bits as possible to the PS and remainder to PL)
	2. I2C EEPROM with 48-bit MAC address (eg. [Microchip 24AA02E48](https://www.microchip.com/en-us/product/24AA02E48))


## ADC Clocking

1. The board should have an option (unpopulated by defualt) to drive the AD9207 CLKP/CLKN and SYSREFP/SYSREFN pins from an external PLL, configured in a similar manner to the 
[AD9082-FMCA-EBZ reference design](https://www.analog.com/media/en/technical-documentation/eval-board-schematic/ad9081-mxfe-fmca-revc-eval-board-schematic.pdf).
	1. resistor / capacitor rotate options should select between PLL CLK/SYSREF signals and backplane CLK/SYSREF signals.
	2. PLL chip (if installed) should have on-board reference, and external (SMP) CLK and SYNC inputs.

## SoM interface

1. The SoM PS should have access to a small I2C EEPROM with 48-bit MAC. Eg [Microchip 24AA02E48](https://www.microchip.com/en-us/product/24AA02E48)
2. The SoM PS should have access to two temperature sensors distributed on the FSM board
3. The SoM should be capable of monitoring current draw from the backplane connector
4. 1 SoM GTY bank should be broken out to a QSFP+ cage. These GTY lanes should be capable of 25Gb/s operation (i.e. 100GbE / 25GbE standards).
5. QSFP cage's I2C interfaces should be connected to PL GPIO pins on the SoM.
6. 8 GTY pins from adjacent banks should be connected to the AD9207 ADC TX pins. These should be capable of operating at 25 Gb/s.
7. A programmable clock generator should be available to the SoM capable of generating GTH/GTY reference clocks for the ADC JESD interface, 100GbE interfaces, and any necessary 1GbE references.
8. 1 red and 1 green LED on front edge of the board should be connected to the SoM *PS* GPIO.
9. 1 red and 1 green LED on the read edge of the board should be connected to the SoM *PS* GPIO.
10. 1 red and 1 green LED on front edge of the board should be connected to the SoM *PL* GPIO.
11. 1 red and 1green LED on the rear edge of the board should be connected to the SoM *PL* GPIO.
